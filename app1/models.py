from __future__ import unicode_literals

from django.db import models

import datetime
import calendar


class User(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=32)
    phone = models.CharField(unique=True, max_length=12)
    created = models.DateTimeField(
        db_index=True, default=datetime.datetime.now)
    updated = models.DateTimeField(db_index=True, auto_now=True)

    def to_dict(self):
        user = {}
        user['id'] = self.id
        user['name'] = self.name
        user['phone'] = self.phone
        user['created'] = calendar.timegm(self.created.utctimetuple())
        user['updated'] = calendar.timegm(self.updated.utctimetuple())

        return user
