import logging
from rest_framework.views import APIView
from django.http import JsonResponse
from django_redis import get_redis_connection

from app1.models import User

logger = logging.getLogger(__name__)


class UserView(APIView):

    def __error_format(self, reason):
        return {'status': 'ERROR', 'reason': reason}

    def get(self, request, *args, **kwargs):

        logger.debug('Http GET content %s' % request.GET)

        result = {'status': 'OK'}

        if 'id' not in request.GET:
            return JsonResponse(self.__error_format('PARAMS_NOT_FOUND'))

        id = request.GET.get('id', None)

        r = get_redis_connection('default')
        if r.exists('%s' % id) :
            user = r.get('%s' % id)
            result['user'] = user
            return JsonResponse(result)

        try:
            user = User.objects.get(id=id)
        except Exception:
            logger.error('User %s is not found' % id)
            return JsonResponse(self.__error_format('USER_IS_NOT_EXIST'))

        result['user'] = user.to_dict()

        r.setex(id, 86400, user.to_dict())

        return JsonResponse(result)

    def post(self, request, *args, **kwargs):
        logger.debug('Http POST content %s' % request.POST)
        result = {'status': 'OK'}

        if 'name' not in request.POST and 'phone' not in request.POST:
            return JsonResponse(self.__error_format('PARAMS_NOT_FOUND'))

        try:
            user, created = User.objects.get_or_create(
                name=request.POST.get('name'), phone=request.POST.get('phone'))
        except Exception as e:
            logger.error('Create user failed %s', e)
            return JsonResponse(self.__error_format('CREATE_FAILED'))

        result['user'] = user.to_dict()

        return JsonResponse(result)
