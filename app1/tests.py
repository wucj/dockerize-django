
from rest_framework.test import APITestCase, APIClient
from django_redis import get_redis_connection

from app1.models import User


class UserTest(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.redis_client = get_redis_connection('default')
        self.user1 = User.objects.create(name='user1', phone='11112222')

    def tearDown(self):
        self.redis_client.flushdb()

    def test_hello(self):
        response = self.client.get('/')
        self.assertEqual(response.content, 'HelloWorld')

    def test_get(self):
        get_data = {}
        get_data['id'] = self.user1.id
        response = self.client.get('/api/user', data=get_data).json()
        self.assertEqual(response['status'], 'OK')
        self.assertEqual(response['user']['id'], self.user1.id)
        self.assertEqual(response['user']['name'], 'user1')
        self.assertEqual(response['user']['phone'], '11112222')

        from_cache = eval(self.redis_client.get('%s' % self.user1.id))
        self.assertEqual(from_cache['id'], self.user1.id)
        self.assertEqual(from_cache['name'], 'user1')
        self.assertEqual(from_cache['phone'], '11112222')

    def test_post_and_get(self):
        post_data = {}
        post_data['name'] = 'user2'
        post_data['phone'] = '22221111'
        response = self.client.post('/api/user', data=post_data).json()
        self.assertEqual(response['status'], 'OK')
        id = response['user']['id']
        get_data = {}
        get_data['id'] = id
        response = self.client.get('/api/user', data=get_data).json()
        self.assertEqual(response['status'], 'OK')
        self.assertEqual(response['user']['id'], id)
        self.assertEqual(response['user']['name'], 'user2')
        self.assertEqual(response['user']['phone'], '22221111')
