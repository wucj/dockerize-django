import logging
from rest_framework.views import APIView
from django.http import HttpResponse


logger = logging.getLogger(__name__)


class HelloView(APIView):

    def get(self, request, *args, **kwargs):
        return HttpResponse('HelloWorld')
