FROM python:2.7

# Install required packages and remove the apt packages cache when done.

RUN apt-get update && apt-get install -y \
    libjpeg-dev \
    redis-tools \
    postgresql-client \
  && rm -rf /var/lib/apt/lists/*

RUN easy_install pip

# install uwsgi now because it takes a little while
COPY ./requirement.txt /tmp/requirement.txt
RUN pip install -r /tmp/requirement.txt

RUN mkdir -p /var/www/mydjango
RUN mkdir -p /var/log/mydjango

COPY ./ /var/www/mydjango/

WORKDIR /var/www/mydjango
