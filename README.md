# dockerize-django

This is an example of dockerized django project



## Development Environment Requirements

1. Install Docker
  Please refer to [Docker](https://docs.docker.com/engine/installation/)


## Start to work
Build docker image
```
docker build -t mydjango .
```

Start up the docker images
```console
$ docker-compose up
```

Get the page on the browser

  - GUI in [here](http://localhost/)


Shutdown the docker images
```console
$ docker-compose down
```

Run test
```console
$ docker-compose run mydjango python manage.py test
```

# APIs

This project contains two Apis
```
GET /
```
This Api will return 'HelloWorld'
 

```
GET   /api/user/
POST  /api/user/
```

This is a restful API which could create a user via POST and get a user via GET 
Please check app1/views.py, POST will store a user to PostgreSQL db and GET will get user by id from Redis if this is not in Redis, will get this user from PostgreSQL

